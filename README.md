# AD tag library

## For developers

To make build use:

`npm i && npm run build`

## For clients

To use the library:

* make container for ad:

`<div id="ad"></div>`

* include library script at the end of the page

`<script type="text/javascript" src="./dist/ad_loader.js"></script>`

* Write the configuration after the library script

```HTML
<script>
    var options = {
        url: "https://pubnative-assets.s3.amazonaws.com/static/adserver/ad_response.json",
        selector: "#ad"
    };    
    AD.load(options);
</script>
```

Where **url** is the link to JSON and **selector** is CSS selector for the ad container.

*__COMMENTS__*

The part of the task I found controversial:

> Every ad must not be rendered more than once. If there are no more ads left from the previous response, the new request to https://pubnative-assets.s3.amazonaws.com/static/adserver/ad_response.json should be initiated.

As soon as the JSON doesn't have some identifier for ad how can I get to know if the ad is unique?
I can use JSON.stringify or convert response to base64 and compare results as strings but I found this pointless.
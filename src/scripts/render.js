import { ADS_TEMPLATE } from "./template";

const renderAd = (ad, selector) => {
    document.querySelector(selector).insertAdjacentHTML("beforeEnd", ad);
};
const getAdOptions = ad => {
    const opt = { link: ad.link };
    ad.assets.map(asset => {
        Object.assign(opt, asset.data);
    });
    return opt;
};
const sendImpression = impression => {
    (new Image).src = impression;
};
export const makeAd = (ad, selector) => {
    const adTag = ADS_TEMPLATE(getAdOptions(ad));
    renderAd(adTag, selector);
    ad.beacons.map(beacon => {
        beacon.type === "impression" && sendImpression(beacon.data.url);
    });
};
export const ADS_TEMPLATE = options => {
    const { link, url, w, h, text } = options;
    return `
        <style>.p_ads a {text-decoration: none; color: #000; font-family: sans-serif;}</style>
        <div class="p_ads">
            <a target="_blank" href="${link}">
                <img src="${url}" width="${w}" height="${h}">
                <div class="pb_title">${text}</div>
            </a>
        </div>`
};
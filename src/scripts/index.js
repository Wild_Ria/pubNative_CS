import { makeAd } from "./render";
import { getData } from "./utils";

const cache = window.localStorage.getItem('json');
const queue = [];

export const load = options => {
    const { url, selector } = options;
    if (cache) {
        JSON.parse(cache).ads.map(ad => {
            makeAd(ad, selector);
        });
    } else {
        if (queue.length > 0) {
            getData(queue[0])
            .then(
                response => {
                    const json = JSON.parse(response);
                    window.localStorage.setItem('json', JSON.stringify(json));
                    json.ads.map(ad => {
                        makeAd(ad, selector);
                    });
                    queue.shift();
                }
            )
            .catch(error => console.error(error));
        } else {
            queue.push(url);
        }        
    }

};